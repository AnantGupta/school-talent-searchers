// School Talent Searcher
// This application is basically designed for those people who are finding
// students who are similarly minded
// Created by Anant Gupta

// Source Code: https://gitlab.com/AnantGupta/school-talent-searchers
// License: MIT License
// Read about the application: README.md in the GitLab repo

// Importing
#include <fstream>
#include <iostream>
using namespace std;

// Creating subject function
void cases(int choice, ofstream &file) {
  switch (choice) {
  case 1:
    file << "Languages";
    break;
  case 2:
    file << "Math";
    break;
  case 3:
    file << "Science";
    break;
  case 4:
    file << "Computer";
    break;
  case 5:
    file << "Arts";
    break;
  case 6:
    file << "Social Science";
    break;
  case 7:
    file << "Sports";
    break;
  case 8:
    file << "Gaming";
    break;
  case 9:
    file << "Book Reading";
    break;
  case 10:
    file << "Other interests";
  default:
    cout << "Wrong number added";
  }
}

// Creating new line function
void NewLine() { cin.ignore(256, '\n'); }

// Main function
int main() {
  int choice;

  cout << "\n\033[1;31mWelcome to School Talent Searcher application\033[0m\n";

  // Infinite loop (until problem occurs or 4 is pressed)
  while (true) {
    cout << "\033[1;33m\nEnter choice:\n\033[0m"
            "\033[1;32m  1 for adding student status\n\033[0m"
            "\033[1;35m  2 for reading about students\033[0m\n"
            "\033[1;36m  3 for deleting previous data\033[0m\n"
            "\033[1;34m  4 for exit\n\n\033[0m";

    cout << "Enter choice: ";
    cin >> choice;

    // If we need to add the student status
    if (choice == 1) {
      string name, notes;
      int grade, choice;
      char yOrN;

      // Name
      cout << "Enter your name: ";
      NewLine();
      getline(cin, name);

      // Grade
      cout << "Enter grade (in number): ";
      cin >> grade;

      // Subject List
      string subjectList =
          "\n\033[1;32m1. Languages\n2. Maths\n3. Science\033[0m\n\033[1;33m4. "
          "Computers\n5. "
          "Arts\n6. Social Science\033[0m\033[1;34m\n7. Sports\n8. "
          "Gaming\n9. Book Reading\033[0m\n\033[1;35m10. Other "
          "interests\033[0m\n\n";

      cout << subjectList;

      // Most liking
      cout << "Enter your most liking from the options above (only 1 option "
              "allowed): ";
      cin >> choice;

      // Open file in append mode & put the content in file
      ofstream file;
      file.open("data.txt", ios_base::app);

      file << "Name: " << name << endl;
      file << "Class: " << grade << endl;
      file << "Area Of Expertise: ";
      cases(choice, file);

      // More likings?
      cout << "Any more subject that you would like to mention?(y/n): ";
      cin >> yOrN;

      // If yes then how many more subject
      if (yOrN == 'y' || yOrN == 'Y') {
        int howManyMoreSubjects;
        cout << "How many more subjects you want to add?: ";
        cin >> howManyMoreSubjects;
        int subjects[howManyMoreSubjects];

        file << "\nMore subjects interested in: ";

        NewLine();

        // Enter subject number
        for (int i = 1; i <= howManyMoreSubjects; i++) {
          cout << subjectList;
          cout << "Enter subject " << i << " number: ";
          cin >> subjects[i - 1];
          cases(subjects[i - 1], file);
          file << ", ";
        }
      }

      // Notes
      cout << "Write some more notes for having more info about you\n";
      NewLine();
      getline(cin, notes);

      file << "\nMore info: " << notes << "\n______________________\n\n";

      cout << "\nThanks for using our service :)\n";
      cout << "__________________\n";
    }

    // If we need to read the data
    else if (choice == 2) {
      cout << endl;
      string text;
      ifstream File("data.txt");
      while (getline(File, text)) {
        cout << text << endl;
      }
      File.close();
      cout << endl;
    }

    // If we need to remove the data
    else if (choice == 3) {
      string password;
      cout << "\nEnter password: ";
      cin >> password;

      // Password verification
      if (password == "anantGupta:)") {
        char choice;
        cout << "\nYou are in\n";
        cout << "Are you sure you want to delete all the existing data in the "
                "file(y/n): ";
        cin >> choice;

        // Sure if we want to delete the file or not
        if (choice == 'y') {
          ofstream deleteFileContent;
          deleteFileContent.open("data.txt");
          cout << "Removed file content\n";
          cout << "____________________\n";
        }

        else {
          cout << "\nNot deleting data\n";
        }
      }

      else {
        cout << "\nWrong password :(\n";
      }
    }

    // Exit
    else if (choice == 4) {
      exit(0);
    }

    // If wrong choice is entered
    else {
      cout << "\nEnter 1, 2, 3 or 4 only\n\n";
    }
  }

  return 0;
}