# About School Talent Searcher

-   This application is designed for those people who are finding
    students who are similarly minded.
-   This helps those people who are interested in finding people and
    making friends who are similarly minded.
-   From here you can add your age also because usually same age people
    who are same minded can be made friends much easier.
-   It can also help teachers who are finding students for various level
    competitions.
-   This will also help teachers find those people who are good in other
    activities other than school curricular activities.
-   Supports all desktop operating systems which is having the C++
    compiler installed.

# Installation

## Install Mingw

Install Mingw for [Windows](https://sourceforge.net/projects/mingw-w64/)

PS: The link is having the Windows version. Linux already has gcc/g++
installed so skip the part. If you have not installed, then install
base-devel

## Install Git

Install [Git](https://git-scm.com/)

PS: On Windows git is not installed. On Linux, if it is not installed
then you can install it with your package manager like pacman, apt, etc

## Clone The Repo

Do a
``` shell
git clone https://gitlab.com/AnantGupta/school-talent-searchers.git
```
after opening the terminal (of your choice) in Linux or Git Bash in
Windows

# Or you can just
Download the zip file on the GitLab page of the program and after downloading it, you can unzip the folder by whatever zip extracter you like

## Run the application

After git cloning the repo... Go in the directory, there do a `g++
linux.cpp` on linux or `g++ windows.cpp` on Windows then a file named a.exe will be formed (in Windows) or a.out
will be formed (in Linux) Run that file with

`.\a.exe` on Windows

or

`./a.out` On Linux

# Application function

-   While running this application we can see the welcome message on the
    terminal window. We can enter 4 options here.
-   1 for adding student status. 2 for viewing all the status 3 for
    deleting the previous data & 4 for exiting from the application.
-   If we choose the 1st option then it will ask for entering the name,
    the grade, and your favorite/expertise area.
-   You can choose 1 of the 13 options listed. After choosing that, you
    would be prompted if you want to add more favorite/expertise areas.
-   If yes then you can do so by following the same process, if no, then
    no problem, you can keep moving ahead.
-   After choosing the options you can write some notes if you want to
    write some. If you are interested in computers so you can list the
    subfields like Cryptocurrencies, programming, etc. Same with other
    areas. After that the thank you message will appear.
-   Now again you can choose the options below as the program is in a
    loop. Now here either you can exit by choosing the third option or
    view the previous student data if you are interested in choosing the
    second option.

# Important Note
If the Linux version is not working for you on either Linux or Mac you can try changing your `terminal emulator` or you can simply run the windows version. The Linux and the Windows version are the same just in the windows version font coloring and bolding is not there.

# Who made this

I (Anant Gupta) made this application from the C++ programming language.

[Check my website](https://anantgupta.gitlab.io/my-website/)

[Check my Video on the same](https://youtu.be/ZsqcZZp9pfE)
